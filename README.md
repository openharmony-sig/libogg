# libogg

## 简介
> libogg是Ogg容器格式库，支持创建、解码和使用Ogg比特流。

> 此文件是OggVorbis软件编解码器源代码的一部分。

![](screenshot/result.gif)

## 下载安装
直接在OpenHarmony-SIG仓中搜索libogg并下载。

## 使用说明
以OpenHarmony 3.1 Beta的rk3568版本为例

1. 将下载的libogg库代码存在以下路径：./third_party/libogg

2. 修改添加依赖的编译脚本，路径：/developtools/bytrace_standard/ohos.build

```

{
  "subsystem": "developtools",
  "parts": {
    "bytrace_standard": {
      "module_list": [
        "//developtools/bytrace_standard/interfaces/innerkits/native:bytrace_core",
        "//developtools/bytrace_standard/bin:bytrace_target",
        "//developtools/bytrace_standard/bin:bytrace.cfg",
        "//developtools/bytrace_standard/interfaces/kits/js/napi:bytrace",
        "//third_party/libogg:libogg"
      ],
      "inner_kits": [
        {
          "type": "so",
          "name": "//developtools/bytrace_standard/interfaces/innerkits/native:bytrace_core",
          "header": {
            "header_files": [
              "bytrace.h"
            ],
            "header_base": "//developtools/bytrace_standard/interfaces/innerkits/native/include"
          }
        }
      ],
      "test_list": [
        "//developtools/bytrace_standard/bin/test:unittest"
      ]
    }
  }
}

```

3. 编译：./build.sh --product-name rk3568 --ccache

4. 生成库文件和一些可执行测试文件，路径：out/rk3568/developtools/profiler

## 接口说明
1. 使用Ogg bitpacking函数初始化为写入的oggpack缓冲区：
   `oggpack_writeinit()`
2. 检查先前使用Ogg bitpacking函数初始化为写入的oggpack缓冲区的就绪状态：
   `oggpack_writecheck()`
3. 将oggpack缓冲区的内容重置为其原始状态：
   `oggpack_reset()`
4. 截断已写入oggpack缓冲区的数据：
   `oggpack_writetrunc()`
5. 将oggpack缓冲区中的零填充到下一个字节边界：
   `oggpack_writealign()`
6. 将位序列从源缓冲区复制到oggpack缓冲区：
   `oggpack_writecopy()`
7. 在写入后清除缓冲区，并释放oggpack缓冲区使用的内存：
   `oggpack_writeclear()`
8. 使用普通缓冲区并准备oggpack缓冲区，以便使用Ogg bitpacking函数进行读取：
   `oggpack_readinit()`
9. 将位写入oggpack缓冲区：
   `oggpack_write()`
10. 查看缓冲区中指定数量的位：
      `oggpack_look()`
11. 查看下一位：
      `oggpack_look1()`
12. 将位置指针提前指定的位数，而不读取任何数据：
      `oggpack_adv()`
13. 将位置指针前进一位，而不读取任何数据：
      `oggpack_adv1()`
14. 从缓冲区读取请求的位数，并前进位置指针：
      `oggpack_read()`
15. 从oggpack缓冲区数据中读取一位，并前进位置指针：
      `oggpack_read1()`
16. 返回oggpack缓冲区中当前访问点后面的总字节数：
      `oggpack_bytes()`
17. 返回oggpack缓冲区内部缓冲区中当前的总位数：
      `oggpack_bits()`
18. 返回指向给定oggpack buffer结构中的数据缓冲区的指针：
      `oggpack_get_buffer()`
19. 将ogg_sync_state结构初始化为已知的初始值，以准备操作ogg比特流：
      `ogg_sync_init()`
20. 检查ogg_sync_state结构：
      `ogg_sync_check()`
21. 释放ogg_sync_state结构的内部存储，并将结构重置为初始状态：
      `ogg_sync_clear()`
22. 销毁ogg_sync_state结构并释放所有使用的内存：
      `ogg_sync_destroy()`
23. 将ogg_sync_state结构的内部计数器重置为初始值：
      `ogg_sync_reset()`
24. 为写入提供适当大小的缓冲区：
      `ogg_sync_buffer()`
25. 告诉ogg_sync_state结构写入缓冲区的字节数：
      `ogg_sync_wrote()`
26. 将ogg_sync_state结构同步到下一个ogg页面：
      `ogg_sync_pageseek()`
27. 获取存储在ogg_sync_state结构缓冲区中的数据，并将其插入ogg页面：
      `ogg_sync_pageout()`
28. 向位流添加完整页面：
      `ogg_stream_pagein()`
29. 组装数据包以输出到编解码器解码引擎：
      `ogg_stream_packetout()`
30. 组装原始数据包并返回：
      `ogg_stream_packetpeek()`
31. 将数据包提交到位流以进行页面封装：
      `ogg_stream_packetin()`
32. 将数据包形成页面：
      `ogg_stream_pageout()`
33. 将数据包形成页面，类似于ogg_stream_pageout()，但允许应用程序显式请求特定的页面溢出大小：
      `ogg_stream_pageout_fill()`
34. 检查流中的剩余数据包，并将剩余数据包强制放入页面：
      `ogg_stream_flush()`
35. 将可用数据包刷新到页面中，类似于ogg_stream_flush() ，但允许应用程序显式请求特定的页面溢出大小：
      `ogg_stream_flush_fill()`
36. 初始化ogg_stream_state结构，并为编码或解码分配适当的内存：
      `ogg_stream_init()`
37. 检查ogg_stream_state结构的错误或准备状态：
      `ogg_stream_check()`
38. 清除和释放ogg_stream_state结构所使用的内部内存，但不会释放结构本身：
      `ogg_stream_clear()`
39. 将ogg_stream_state结构中的值设置回初始值：
      `ogg_stream_reset()`
40. 重新初始化ogg_stream_state状态中的值，就像ogg_stream_reset()一样：
      `ogg_stream_reset_serialno()`
41. 释放ogg_stream_state结构使用的内部内存以及结构本身：
      `ogg_stream_destroy()`
42. 返回此页面中使用的ogg页面的版本：
      `ogg_page_version()`
43. 指示当前页是否包含从上一页继续的数据包数据：
      `ogg_page_continued()`
44. 返回此页上完成的数据包数：
      `ogg_page_packets()`
45. 指示此页是否位于逻辑位流的开头：
      `ogg_page_bos()`
46. 指示此页是否位于逻辑位流的末尾：
      `ogg_page_eos()`
47. 返回此页末尾包含的数据包数据的精确粒度位置：
      `ogg_page_granulepos()`
48. 返回此页的逻辑位流的唯一序列号：
      `ogg_page_serialno()`
49. 返回连续页码：
      `ogg_page_pageno()`
50. 清除ogg数据包结构使用的内存，但不会释放结构本身：
      `ogg_packet_clear()`
51. 对ogg页进行校验：
      `ogg_page_checksum_set()`

## 约束与限制

在下述版本验证通过：

DevEco Studio 版本：3.1 Beta1(3.1.0.200)，SDK:API9 Beta5(3.2.10.6)

## 目录结构
````
|---- libogg
|     |---- doc                          #Ogg规范和libogg API文档
|     |---- include                      #头文件
|     |---- src                          #libogg的源，实现公共域Ogg比特流格式
|           |---- bitwise.c              #将大小可变的单词打包到八位字节流中
|           |---- framing.c              #将原始数据包编码到框架OggSquish流中，并将Ogg流解码回原始数据包
|     |---- win32                        #win32项目和生成自动化
|     |---- README.md                    #安装使用方法
````

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/libogg/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/openharmony-sig/libogg/pulls) 。

## 开源协议
本项目基于 [BSD-3-Clause license](https://gitee.com/openharmony-sig/libogg/blob/master/COPYING) ，请自由地享受和参与开源。
